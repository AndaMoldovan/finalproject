import React from 'react';
import {ReservationTableLine} from './ReservationTableLine';
import { Box, Text, Button } from 'grommet';
import "./style/_all.css"
import {FormPreviousLink} from "grommet-icons/icons/FormPreviousLink";
import MostUsedChart from './MostUsedChart';


export default class MainReservationUser extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            reservations: [],
            chart: ''
        }
    }

    componentWillMount(){
        let username = "user";
        let password = "11";
        let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
        fetch( "http://localhost:8080/base/reservations/getAllReservations", {
            method: 'GET',
            headers: {
                'Authorization': bearer,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then( (response) => response.json())
            .then( (body) => {
                //console.log(body);
                this.setState({reservations: body});
            });
    }

    handleCreateReservation = () => {
      window.location = "http://localhost:3000/user/reservations/createReservation/" + this.props.match.params.id;
    };

    handleGoBack = () => {
        window.location = "http://localhost:3000/user/" + this.props.match.params.id;
    }

    handleSportChart = (type) => {
        console.log("CLICK");
        // chart = <MostUsedChart type={type} />
    }

    render(){

        let userReservations = [];
        this.state.reservations ?  this.state.reservations.map(reservation => {
            if(reservation.userDTO.userId === parseInt(this.props.match.params.id)){
                userReservations.push(reservation);
            }
        }) : userReservations = null;

        let chartType = null;
        if(this.state.chart === "sport"){
            chartType = <MostUsedChart type={"sport"} />
        }else if(this.state.chart === "facility"){
            chartType = <MostUsedChart type={"facility"} />
        }

        console.log(this.state.chart)
        return(
            <Box direction={"column"}>
                <Button id="facilities-go-back" icon={<FormPreviousLink />} onClick={this.handleGoBack} />
                <Box id={"user-reservation-text"}>
                    <Text alignSelf={"center"} color={"neutral-3"} size={"xlarge"}> Manage Your Reservations </Text>
                </Box>
                <Box>
                    <ReservationTableLine reservations={userReservations} />
                </Box>
                <Box id={"user-create-reservation-box"}>
                    <Button id="user-create-reservation-btn" onClick={this.handleCreateReservation}> Create a Reservation </Button>
                </Box>
                <Box id={"view-reservations-chart"} direction={"row"}>
                    <Button id={"view-sport-chart-box"} onClick={() => {this.setState({chart: "sport"})}}> View the most looked at sports </Button>
                    <Button id={"view-facility-chart-box"} onClick={() => {this.setState({chart: "facility"})}}> View the most looked at sport facilities </Button>
                </Box>
                <Box id={"chart-box"}>
                    {chartType}
                </Box>
            </Box>
        );
    }
}