import React from 'react'
import {
    Box,
    Table,
    TableBody,
    TableCell,
    TableHeader,
    TableRow,
    Text
} from "grommet";

const COLUMNS = [
    {
        property: "sportFacilityDTO.sportDTO.name",
        label: "Sport Name"
    },
    {
        property: "sportFacilityDTO.facilityDTO.name",
        label: "Facility"
    },
    {
        property: "noPeople",
        label: "No. People"
    },
    {
        property: "startDate",
        label: "Start Date"
    },
    {
        property: "endDate",
        label: "End Date"
    }
];

export function ReservationTableLine(props){
    const reservations = props.reservations;

    let tableLine = <Table caption={"Reservation line"}>
                        <TableHeader>
                            <TableRow>
                                {COLUMNS.map(c => (
                                    <TableCell key={c.property} scope={"col"}>
                                        <Text>{c.label}</Text>
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHeader>
                        <TableBody>
                            {reservations.map(r => (
                                <TableRow key={r.id}>
                                    <TableCell key={r.sportFacilityDTO.sportDTO.name}>
                                        <Text>{r.sportFacilityDTO.sportDTO.name}</Text>
                                    </TableCell>
                                    <TableCell key={r.sportFacilityDTO.facilityDTO.name}>
                                        <Text>{r.sportFacilityDTO.facilityDTO.name}</Text>
                                    </TableCell>
                                    <TableCell key={r.noPeople}>
                                        <Text>{r.noPeople}</Text>
                                    </TableCell>
                                    <TableCell key={r.startDate}>
                                         <Text>{r.startDate}</Text>
                                    </TableCell>
                                    <TableCell key={r.endDate}>
                                        <Text>{r.endDate}</Text>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>;

    return(
        <Box>
            {tableLine}
        </Box>
    );
}