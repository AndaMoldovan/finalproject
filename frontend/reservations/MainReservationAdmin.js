import React from 'react';
import ReservationsTable from './ReservationsTable';
import {Box, Button} from "grommet";
import {FormPreviousLink} from "grommet-icons/icons/FormPreviousLink";
import {ReservationOptions} from "./ReservationOptions";


function handleGoBack(){
    window.location = "http://localhost:3000/admin";
}

export default function MainReservationAdmin(props){
    return(
        <Box direction={"column"}>
            <Button id="facilities-go-back" icon={<FormPreviousLink />} onClick={() => handleGoBack()} />
            <Box>
                <ReservationsTable />
            </Box>
            <Box>
                <ReservationOptions />
            </Box>
        </Box>
    );
}