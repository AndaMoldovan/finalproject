import React from 'react'
import { Box, Button, Accordion, AccordionPanel } from 'grommet'
import {ReservationTableLine} from './ReservationTableLine';


const AccordionConstruct = props =>  {
    const {reservations, animate, multiple, ...rest} = props;
    console.log("aa");
    reservations.map(e => {
        console.log("Reservation");
        console.log(e);
    });

    return(
        <Box {...rest}>
            <Accordion animate={animate} multiple={multiple}>
                {(reservations !== undefined) && reservations.map(el => {
                    let label = "Reservation no." + el.id + " made by: " + el.userDTO.userName;
                    let r = [el];
                    return (
                            <AccordionPanel label={label}>
                                <Box background={"light"} style={{height: "100px"}}>
                                    <ReservationTableLine reservations={r} />
                                </Box>
                            </AccordionPanel>
                    )
                })}
            </Accordion>
        </Box>
    );
};

export default class ReservationsTable extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            reservations: []
        }
    }

    componentWillMount(){
        console.log("COMPONENT WILL MOUNT");
        let username = "admin";
        let password = "11";
        let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
        fetch( "http://localhost:8080/base/reservations/getAllReservations", {
        method: 'GET',
        headers: {
            'Authorization': bearer,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        }).then( (response) => response.json())
            .then( (body) => {
                console.log(body);
                this.setState({reservations: body});
            });
    }

    render(){

        let acc = (this.state.reservations !== undefined ) ? <AccordionConstruct reservations={this.state.reservations} animate={false} multiple={false} /> : null;

        return(
            <Box align={"centre"} pad={"large"}>
                {acc}
            </Box>
        );
    }
}