import React from 'react';
import { useState, useEffect } from 'react';
import { Text, Box, Button, TextInput,  Calendar, MaskedInput, Select } from 'grommet';
import {FormPreviousLink} from "grommet-icons/icons/FormPreviousLink";

const maskedInputProperties = [
    {
        length: [1, 2],
        options : [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"],
        regExp: /^[1-2]$|^[0-9]$/,
        placeholder: "hh"
    },
    { fixed: ":" },
    {
        length: 2,
        options: ["00", "15", "30", "45"],
        regExp: /^[0-5][0-9]$|^[0-9]$/,
        placeholder: "mm"
    },
    { fixed : ":00"}
];

function findSport(sports, name){
    let sport;
    sports.map(el => {
        if(el.name === name) sport = el;
    });
    return sport;
}

function findFacility(facilities, name){
    let facility;
    facilities.map(el => {
        if(el.name === name) facility = el;
    });
    return facility;
}

function formatDate(date, time){
    let fin = date.substring(0, 10);
    fin = fin + ' ' + time;
    return fin;
}

function getSportFacility(sport, facility, sportFacilities){
    let res;
    sportFacilities.map(el => {
        if(el.sportDTO.sportId === sport.sportId && el.facilityDTO.id === facility.id){
            res = el;
        }
    });
    return res;
}

function handleCreateReservation(startDate, startTime, endTime, people, facilityName, sportName, sports, facilities, sportFacilities){
    let stDate = formatDate(startDate, startTime);
    let finDate = formatDate(startDate, endTime);
    let sport = findSport(sports, sportName);
    let facility = findFacility(facilities, facilityName.split('-')[0]);

    let sportFacility = getSportFacility(sport, facility, sportFacilities);


    let username = "user";
    let password = "11";
    let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
    fetch( "http://localhost:8080/base/reservations/saveReservation", {
        method: 'POST',
        headers: {
            'Authorization': bearer,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "id": 0,
            "startDate": stDate,
            "endDate": finDate,
            "noPeople": parseInt(people),
            "userDTO": {
                "userId": 5,
                "userName": "User",
                "email": "user",
                "password": "11",
                "roleDTO": {
                    "roleId": 2,
                    "role": "USER"
                }
            },
            "sportFacilityDTO": {
                "id": sportFacility.id,
                "sportDTO": {
                    "sportId": sportFacility.sportDTO.sportId,
                    "name": sportFacility.sportDTO.name,
                    "description": sportFacility.sportDTO.description,
                    "noPeople": sportFacility.sportDTO.noPeople,
                    "equipment": sportFacility.sportDTO.equipment
                },
                "facilityDTO": {
                    "id": sportFacility.facilityDTO.id,
                    "name": sportFacility.facilityDTO.name,
                    "description": sportFacility.facilityDTO.description,
                    "address": sportFacility.facilityDTO.address
                }
            }
        })
    }).then( (response) => response.json()).then( body => console.log(body))
}



export default function CreateReservation(props){

    const [people, setPeople] = useState();
    const [startDate, setStartDate] = useState();
    const [endDate, setEndDate] = useState();
    const [startTime, setStartTime] = useState();
    const [endTime, setEndTime] = useState();
    const [selectedSport, setSelectedSport] = useState();
    const [selectedFacility, setSelectedFacility] = useState();
    const [sportValue, setSportValue] = useState();
    const [facilityValue, setFacilityValue] = useState();
    const [sports, setSports] = useState();
    const [facilities, setFacilities] = useState();
    const [sportFacilities, setSportFacilities] = useState();

    useEffect(() =>{
        let username = 'user';
        let password = '11';
        let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
        fetch( "http://localhost:8080/base/sport/getAllSports", {
            // mode: 'no-cors',
            method: 'GET',
            headers: {
                'Authorization': bearer,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then( (response) => response.json())
            .then( (body) => {
                setSports(body);
            });

        fetch( "http://localhost:8080/base/facilities/getAllFacilities", {
            // mode: 'no-cors',
            method: 'GET',
            headers: {
                'Authorization': bearer,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then( (response) => response.json())
            .then( (body) => {
                setFacilities(body);
            });

        fetch( "http://localhost:8080/base/sportFacility/getAllSportFacilities", {
            // mode: 'no-cors',
            method: 'GET',
            headers: {
                'Authorization': bearer,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then( (response) => response.json())
            .then( (body) => {
                setSportFacilities(body);
            });
    }, []);

    let sp = [], fac = [];
    if(sports){
        sports.map(el => {sp.push(el.name)});
    }
    if(facilities){
        facilities.map(el => {fac.push(el.name + '-' + el.address)})
    }

    function handleGoBack(){
        window.location = "http://localhost:3000/user/reservations/" + props.match.params.id;
    }

    return(
        <Box direction={"column"}>
            <Button id="facilities-go-back" icon={<FormPreviousLink />} onClick={() => handleGoBack()} />
            <Box id="reservation-title">
                <Text alignSelf={"center"} color={"neutral-3"} size={"xlarge"}> Create a Reservation </Text>
            </Box>
            <Box direction={"row"}>
                <Box direction={"column"}>
                    <Box id='start-date-box'>
                        <Text id="title" size={"large"}> Start Date </Text>
                        <Calendar id="start-date-calendar" date={startDate} onSelect={(newDate) => setStartDate(newDate)} size={"small"} firstDayOfWeek={1} />
                    </Box>
                </Box>
                <Box id="time-box" direction={"column"}>
                    <Box id="start-time">
                        <Text id="title" size={"large"}> Start Time </Text>
                        <MaskedInput mask={maskedInputProperties} value={startTime} onChange={(event) => setStartTime(event.target.value)}/>
                    </Box>
                    <Box id="end-time">
                        <Text id="title" size={"large"}> End Time </Text>
                        <MaskedInput mask={maskedInputProperties} value={endTime} onChange={(event) => setEndTime(event.target.value)}/>
                    </Box>
                </Box>
                <Box direction={"column"}>
                    <Box id="sports-combo-box">
                        <Text id="title" size={"large"}> Sport </Text>
                        <Select selected={selectedSport} value={sportValue} onChange={ event => (
                            setSelectedSport(event.selected),
                            setSportValue(event.value)
                        )} options={sp} />
                    </Box>
                    <Box id="facilities-combo-box">
                        <Text id="title" size={"large"}> Facility </Text>
                        <Select selected={selectedFacility} value={facilityValue} onChange={ event => (
                            setSelectedFacility(event.selected),
                            setFacilityValue(event.value)
                        )} options={fac} />
                    </Box>
                    <Box id="people-range-selector">
                        <Text id="title" size={"large"}> No. People </Text>
                        <TextInput type={"text"} value={people} onChange={(event) => setPeople(event.target.value)} />
                    </Box>
                </Box>
                <Box direction={"column"}>
                    <Box id="reservation-button">
                        <Button id="create-reservation-button" onClick={() => handleCreateReservation(startDate, startTime, endTime, people, facilityValue, sportValue, sports, facilities, sportFacilities)}> Create Reservation </Button>
                    </Box>
                </Box>
            </Box>
        </Box>
    );
}