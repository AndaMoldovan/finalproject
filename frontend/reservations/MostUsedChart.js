import React from 'react';
import { useState, useEffect } from 'react';
import {Box, Button, Chart} from 'grommet';
import {LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip} from "recharts";

function filterBySport(reservations, type){
    let objects = [];
    reservations.map(reservation => {
        let times = 0, exists = false;
        if(type === "sport") {
            console.log("HERE 1")
            objects.map(obj => {
                (obj.name === reservation.sportFacilityDTO.sportDTO.name) ? exists = true : exists = false;
            });
        }else if(type === "facility"){
            objects.map(obj => {
                (obj.name === reservation.sportFacilityDTO.facilityDTO.name) ? exists = true : exists = false;
            });
        }

        if(exists === false) {
            reservations.map(el => {
                if(type === "sport"){
                    console.log("HERE 2")
                    if (el.sportFacilityDTO.sportDTO.name === reservation.sportFacilityDTO.sportDTO.name) {
                        times++;
                    }
                }else if(type === "facility"){
                    if (el.sportFacilityDTO.facilityDTO.name === reservation.sportFacilityDTO.facilityDTO.name) {
                        times++;
                    }
                }
            });
            if(type === "sport"){
                console.log("HERE 3")
                objects.push({name: reservation.sportFacilityDTO.sportDTO.name, times: times});
            }else if(type === "facility"){
                objects.push({name: reservation.sportFacilityDTO.facilityDTO.name, times: times});
            }
        }
        exists = false;
    });
    console.log(objects);
    return objects;
}


export default function MostUsedChart(props){
    const [reservations, setReservations] = useState();

    useEffect(() => {
        let username = 'admin';
        let password = '11';
        let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
        fetch( "http://localhost:8080/base/reservations/getAllReservations", {
            // mode: 'no-cors',
            method: 'GET',
            headers: {
                'Authorization': bearer,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then( (response) => response.json())
            .then( (body) => {
                setReservations(body);
            });
    }, []);

    let type = props.type;
    let data = (reservations) ? filterBySport(reservations, type) : null;

    console.log("DATA");
    console.log(data)
    return(
       <Box>
           <LineChart width={600} height={300} data={data} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
               <Line type="monotone" dataKey="times" stroke="#8884d8" />
               <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
               <XAxis dataKey="name" />
               <YAxis />
               <Tooltip />
           </LineChart>
       </Box>
    );
}