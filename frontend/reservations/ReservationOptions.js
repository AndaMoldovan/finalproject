import React from 'react';
import { useState } from 'react'
import { Box, Button } from 'grommet';
import {ConfirmationToast, UpdateSlider} from './Slider';
import './style/_all.css';

const buttonStyle = {
    "side" : "all",
    "color" : "red",
    "size" : "small"
};

export function ReservationOptions(){
    const [option, setOption] = useState('none');

    return(
        <Box direction={"row"}>
            <Box border={buttonStyle} id="update-reservation">
                <Button onClick={() => setOption('update')}> Update Reservation </Button>
            </Box>
            <Box border={buttonStyle} id="delete-reservation">
                <Button onClick={() => setOption('delete')}> Delete Reservation </Button>
            </Box>
            {(option === 'delete') && <ConfirmationToast onClose={() => setOption('none')} />}
            {(option === 'update') && <UpdateSlider onClose={() => setOption('none')} />}
        </Box>
    );
}