import React from 'react';
import {useState} from 'react';
import {Layer, Text, Box, Button, TextInput, FormField} from 'grommet';

export function ConfirmationToast({ onClose }){

    const [id, setId] = useState(-1);

    function handleYes() {
        let username = "admin";
        let password = "11";
        let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
        fetch( "http://localhost:8080/base/reservations/deleteReservation?id=" + Number(id), {
            method: 'DELETE',
            headers: {
                'Authorization': bearer,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then( (response) => response.json()).then( body => console.log(body));

        window.location = "http://localhost:3000/admin/reservations";
    }

    let message = <Layer position={"top"} onClickOutside={onClose}>
                    <Box pad={"large"} gap={"medium"}>
                        <Text> Delete the reservation? </Text>
                        <FormField label="Id">
                            <TextInput type="text" value = {id} onChange={ (event) => setId(event.target.value)}/>
                        </FormField>
                        <Button label={"Yes"} onClick={() => handleYes()}> Yes </Button>
                        <Button label={"No"} onClick={() => onClose()}> No </Button>
                    </Box>
                  </Layer>;

    return(
        <Box>
            {message}
        </Box>
    );
}

export function UpdateSlider({onClose}){

        const [id, setId] = useState(-1);
        const [startDate, setStartDate] = useState('');
        const [endDate, setEndDate] = useState('');
        const [people, setPeople] = useState(0);
        const [currentReservation, setCurrentReservation] = useState();

        function handleUpdate(){
            let username = "admin";
            let password = "11";
            let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
            fetch( "http://localhost:8080/base/reservations/getReservationById?id=" + Number(id), {
                method: 'GET',
                headers: {
                    'Authorization': bearer,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then( (response) => response.json())
                .then( body => {
                    setCurrentReservation(body);
                    console.log(body)
                       let username = "admin";
                       let password = "11";
                       let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
                       fetch("http://localhost:8080/base/reservations/updateReservation", {
                           // mode: 'no-cors',
                           method: 'PUT',
                           headers: {
                               'Authorization': bearer,
                               'Accept': 'application/json',
                               'Content-Type': 'application/json',
                           },
                           body: JSON.stringify({
                               "id": id,
                               "startDate": startDate,
                               "endDate": endDate,
                               "noPeople": people,
                               "userDTO": {
                                   "userId": body.userDTO.id,
                                   "userName": body.userDTO.userName,
                                   "email": body.userDTO.email,
                                   "password": body.userDTO.password,
                                   "roleDTO": {
                                       "roleId": body.userDTO.roleDTO.id,
                                       "role": body.userDTO.roleDTO.role
                                   }
                               },
                               "sportFacilityDTO": {
                                   "id": body.sportFacilityDTO.id,
                                   "sportDTO": {
                                       "sportId": body.sportFacilityDTO.sportDTO.id,
                                       "name": body.sportFacilityDTO.sportDTO.name,
                                       "description": body.sportFacilityDTO.sportDTO.description,
                                       "noPeople": body.sportFacilityDTO.sportDTO.noPeople,
                                       "equipment": body.sportFacilityDTO.sportDTO.equipment
                                   },
                                   "facilityDTO": {
                                       "id": body.sportFacilityDTO.facilityDTO.id,
                                       "name": body.sportFacilityDTO.facilityDTO.name,
                                       "description": body.sportFacilityDTO.facilityDTO.description,
                                       "address": body.sportFacilityDTO.facilityDTO.address
                                   }
                               }
                           })
                       }).then((response) => response.json()).then(body => {
                           console.log("UPDATE RESPONSE");
                           console.log(body)
                       });
                });
        }


        return(
            <Layer position={"top"} full={"horizontal"} modal={false} plain={"true"} onEsc={() => onClose}>
                <Box background={"brand"} fill={"vertical"}>
                    <Box pad={{horizontal: 'medium', vertical: 'small'}}>
                        <Text size={"large"}> Update a reservation </Text>
                    </Box>
                    <Box>
                        <FormField label={"Id"}>
                            <TextInput type={"text"} value={id} onChange={(event) => setId(event.target.value)} />
                        </FormField>
                        <FormField label={"Start Date"}>
                            <TextInput type={"text"} value={startDate} onChange={(event) => setStartDate(event.target.value)} />
                        </FormField>
                        <FormField label={"End Date"}>
                            <TextInput type={"text"} value={endDate} onChange={(event) => setEndDate(event.target.value)} />
                        </FormField>
                        <FormField label={"No. People"}>
                            <TextInput type={"text"} value={people} onChange={(event) => setPeople(event.target.value)} />
                        </FormField>
                        <Button onClick={() => handleUpdate()}> Update </Button>
                        <Button onClick={ onClose}> Done </Button>
                    </Box>
                </Box>
            </Layer>
        );
}

