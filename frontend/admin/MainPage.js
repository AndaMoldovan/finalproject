import React from 'react';
import { Box, Menu, Button } from 'grommet';
import {User} from 'grommet-icons/icons/User'
import {Organization} from 'grommet-icons/icons/Organization';
import {Schedule} from 'grommet-icons/icons/Schedule';
import { Run } from 'grommet-icons/icons/Run';


export default class AdminPage extends React.Component{

    handleLogOut = () => {
        console.log("logout");
        window.location = "http://localhost:3000/login";
    };

    handleGoToFacilities = () => {
        console.log("facilities");
        window.location = "http://localhost:3000/admin/facilities";
    };

    handleGoToReservations = () => {
        console.log("freservations");
        window.location = "http://localhost:3000/admin/reservations";
    };

    handleGoToSports = () => {
        window.location = "http://localhost:3000/admin/sports";
    };

    render(){
        return(
            <Box direction="column">
                <Box direction="row">
                    <Box>
                        <Menu icon={<User />} items={[ {label: "logout", onClick: this.handleLogOut}]} />
                    </Box>
                    <Box>
                        <Menu icon={<Organization />} items={[ {label: "facilities", onClick: this.handleGoToFacilities}]} />
                    </Box>
                    <Box>
                        <Menu icon={<Schedule />} items={[ {label: "reservations", onClick: this.handleGoToReservations}]} />
                    </Box>
                    <Box>
                        <Menu icon={<Run />} items={[ {label: "sports", onClick: this.handleGoToSports} ]} />
                    </Box>
                </Box>
            </Box>
        );
    }
}