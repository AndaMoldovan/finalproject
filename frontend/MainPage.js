import React from 'react';
import { Box, Button, Carousel, Text } from 'grommet';
import { TreeOption,  Achievement, Aed, Bike, WheelchairActive, Yoga } from "grommet-icons";
import './MainPage.css';

export default class MainPage extends React.Component{

    handleLogin = () => {
        window.location = "http://localhost:3000/login";
    };

    handleRegister = () => {
        window.location = "http://localhost:3000/register"
    };

    render(){
        return(
            <Box>
                <Text alignSelf={"center"} color={"neutral-3"} size={"xxlarge"}> Welcome! </Text>
                <Box align={"center"} pad={"large"}>
                    <Carousel>
                        <Box pad={"xlarge"} background={"accent-1"}>
                            <TreeOption size={"xlarge"} />
                        </Box>
                        <Box pad={"xlarge"} background={"accent-1"}>
                            <Achievement size={"xlarge"} />
                        </Box>
                        <Box pad={"xlarge"} background={"accent-1"}>
                            <Aed size={"xlarge"} />
                        </Box>
                        <Box pad={"xlarge"} background={"accent-1"}>
                            <Bike size={"xlarge"} />
                        </Box>
                        <Box pad={"xlarge"} background={"accent-1"}>
                            <WheelchairActive size={"xlarge"} />
                        </Box>
                        <Box pad={"xlarge"} background={"accent-1"}>
                            <Yoga size={"xlarge"} />
                        </Box>
                    </Carousel>
                </Box>
                <Box direction={"row"}>
                    <Box id={"login-box"}>
                        <Button id="login-button" onClick={this.handleLogin}> Go to Login </Button>
                    </Box>
                    <Box id={"register-box"}>
                        <Button id="register-button" onClick={this.handleRegister}> Register </Button>
                    </Box>
                </Box>
            </Box>
        );
    }
}