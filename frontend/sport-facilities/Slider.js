import React from 'react';
import { useState } from 'react';
import {Box, Button, FormField, Layer, Text, TextInput} from 'grommet';
import { Close } from 'grommet-icons/icons/Close';

function handleAddFacility(name, address, description){
    let username = "admin";
    let password = "11";
    let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
    fetch( "http://localhost:8080/base/facilities/saveFacility", {
        // mode: 'no-cors',
        method: 'POST',
        headers: {
            'Authorization': bearer,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "id" : 0,
            "name": name.toString(),
            "description": description.toString(),
            "address": address.toString()
        })
    }).then( (response) => response.json()).then( body => console.log(body))
}

function handleUpdateFacility(name, address, description){
    let username = "admin";
    let password = "11";
    let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
    fetch( "http://localhost:8080/base/facilities/updateFacility", {
        // mode: 'no-cors',
        method: 'PUT',
        headers: {
            'Authorization': bearer,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "id" : -1,
            "name": name.toString(),
            "description": description.toString(),
            "address": address.toString()
        })
    }).then( (response) => response.json()).then( body => console.log(body))
}

function handleDeleteFacility(address) {
    let username = "admin";
    let password = "11";
    let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
    fetch( "http://localhost:8080/base/facilities/deleteFacilityByAddress?address=" + address, {
        // mode: 'no-cors',
        method: 'DELETE',
        headers: {
            'Authorization': bearer,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    }).then( (response) => response.json()).then( body => console.log(body))

}

export function LayerSlider ( { onClose, action} ) {
    const [name, setName] = useState('');
    const [address, setAddress] = useState('');
    const [description, setDescription] = useState('');

    let event;
    let title;
    if(action === "add"){
        event = <Button onClick={() => handleAddFacility(name, address, description)}> Add </Button>;
        title = "Add";
    }else if(action === "update"){
        event = <Button onClick={() => handleUpdateFacility(name, address, description)}> Update </Button>
        title = "Update";
    }else if (action === "delete"){
        event = <Button onClick={() => handleDeleteFacility(address)}> Delete </Button>
        title = "Delete";
    }

    return(
        <Layer position={"left"} full={"vertical"} modal={false} plain={"true"}>
            <Box background={"brand"} fill={"vertical"}>
                <Box pad={{horizontal: 'medium', vertical: 'small'}}>
                    <Text size={"large"}> {title} a Facility </Text>
                </Box>
                <Box>
                    {(action !== 'delete') && <FormField label="Name">
                        <TextInput type="text" value = {name} onChange={ (event) => setName(event.target.value)}/>
                    </FormField>}
                    <FormField label="Address">
                        <TextInput type="text" value = {address} onChange={ (event) => setAddress(event.target.value)}/>
                    </FormField>
                    {(action !== 'delete') && <FormField label="Description">
                        <TextInput type="text" value = {description} onChange={ (event) => setDescription(event.target.value)}/>
                    </FormField>}
                    {event}
                    <Button onClick={onClose}> Done </Button>
                </Box>
            </Box>
        </Layer>
    );
}

export const Toast = ({ onClose }) => (
    <Layer modal={false} full='horizontal' position='top'>
        <Box
            background='light-3'
            pad='small'
            direction='row'
            justify='between'
            align='center'
        >
            <Text size='large'>I have a message for you.</Text>
            <Button icon={<Close />} onClick={onClose} />
        </Box>
    </Layer>
);
