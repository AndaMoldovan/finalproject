import React from 'react';
import { Box, Button } from 'grommet';
import SportFacilities from './SportFacilities';
import FacilityOptions from './FacilityOptions';
import { FormPreviousLink } from 'grommet-icons/icons/FormPreviousLink';

function handleGoBack(){
    window.location = "http://localhost:3000/admin";
}

export default function MainFacility(props){
    return(
        <Box direction={"column"}>
            <Button id="facilities-go-back" icon={<FormPreviousLink />} onClick={() => handleGoBack()} />
            <Box>
                <SportFacilities />
            </Box>
            <Box>
               <FacilityOptions />
            </Box>
        </Box>
    );
}