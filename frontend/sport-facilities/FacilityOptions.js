import React from 'react';
import { useState } from 'react'
import { Box, Button } from 'grommet';
import { LayerSlider} from "./Slider";
import './style/_all.css';

const buttonStyle = {
    "side" : "all",
    "color" : "red",
    "size" : "small"
};

function FacilityOptions(){

     const [option, setOption] = useState('none');

    return(
        <Box direction={"row"}>
            <Box border={buttonStyle} id="add-facility">
                <Button onClick={() => setOption('add')}> Add Facility </Button>
            </Box>
            <Box border={buttonStyle}  id="update-facility">
                <Button onClick={() => setOption('update')}> Update Facility </Button>
            </Box>
            <Box border={buttonStyle}  id="delete-facility">
                <Button onClick={() => setOption('delete')}> Delete Facility </Button>
            </Box>
            {(option === 'add') && <LayerSlider onClose={() => setOption('none')} action={"add"}/>}
            {(option === 'update') && <LayerSlider onClose={() => setOption('none')} action={"update"}/>}
            {(option === 'delete') && <LayerSlider onClose={() => setOption('none')} action={"delete"}/>}
        </Box>
    );
}

export default FacilityOptions;