import React from 'react'
import { Box} from 'grommet';
import {
    Table,
    TableBody,
    TableCell,
    TableHeader,
    TableRow,
    Text
} from "grommet";

const COLUMNS = [
    {
        property: 'name',
        label: 'Name',
        dataScope: 'row',
        format: datum => <Text weight={"bold"}>{datum.name}</Text>
    },
    {
        property: 'address',
        label: 'Address',
    },
    {
        property: 'description',
        label: 'Description',
    },
];

export default class SportFacilities extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            facilities: []
        }
    }

    componentWillMount() {
        let username = "admin";
        let password = "11";
        let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
        fetch( "http://localhost:8080/base/facilities/getAllFacilities", {
            // mode: 'no-cors',
            method: 'GET',
            headers: {
                'Authorization': bearer,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then( (response) => response.json())
            .then( (body) => {
                this.setState({facilities: body});
            });
    }

    render(){
        // this.state.facilities.map(fa => {
        //     console.log(fa.name);
        // });
        let fac = [];
        this.state.facilities ? fac = this.state.facilities : fac = [];

        let table = <Table caption={"Facility Table"}>
                        <TableHeader>
                            <TableRow>
                                {COLUMNS.map( c => (
                                    <TableCell key={c.property} scope={"col"}>
                                        <Text>{c.label}</Text>
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHeader>
                        <TableBody>
                            {fac.map(datum => (
                                <TableRow key={datum.id}>
                                    {COLUMNS.map(c => (
                                        <TableCell key={c.property}>
                                            <Text>{datum[c.property]}</Text>
                                        </TableCell>
                                    ))}
                                </TableRow>
                            ))}
                        </TableBody>
                     </Table>;

        return(
            <Box align={"centre"} pad={"large"}>
                {table}
            </Box>
        );
    }
}
