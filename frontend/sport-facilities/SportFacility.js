import React from 'react';
import { Box } from 'grommet';

export default function SportFacility(props){
    return(
        <Box direction={"column"}>
            <Box direction={"row"}>
                <Box>
                    <h2> {props.name} </h2>
                </Box>
                <Box>
                    <h2> {props.description} </h2>
                </Box>
            </Box>
            <Box>
                <h2> {props.address} </h2>
            </Box>
        </Box>
    );
};