import React, { Component } from 'react';
import './MainPage.css';
import Login from './login/Login';
import { Box } from 'grommet';
import {Route, Switch} from "react-router-dom";
import MainPage from "./MainPage";
import Register from './register/Register';
import AdminPage from './admin/MainPage';
import UserPage from './user/MainPage';
import MainFacility from './sport-facilities/MainFacility';
import MainReservationAdmin from './reservations/MainReservationAdmin';
import MainSport from './sports/MainSport';
import MainReservationUser from "./reservations/MainReservationUser";
import  CreateReservation from './reservations/CreateReservation';

class App extends Component {
  render() {
    return (

     <Box>
         <Switch>
             <Route exact path='/' component={MainPage} />
             <Route path='/login' component={Login} />
             <Route path='/register' component={Register} />
             <Route exact path='/admin' component={AdminPage} />
             <Route exact path='/user/:id' component={UserPage} />
             <Route path='/admin/facilities' component={MainFacility} />
             <Route path='/admin/reservations' component={MainReservationAdmin} />
             <Route exact path='/user/reservations/:id' component={MainReservationUser} />
             <Route path='/user/reservations/createReservation/:id' component={CreateReservation} />
             <Route path='/admin/sports' component={MainSport} />

         </Switch>
     </Box>
    );
  }
}

export default App;
