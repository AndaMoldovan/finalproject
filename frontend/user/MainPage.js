import React from 'react';
import {Box, Menu, Text, Button} from 'grommet';
import {User} from "grommet-icons/icons/User";
import MostUsedChart from '../reservations/MostUsedChart';
import './style/main-page.css';

export default class UserPage extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            user: null
        }
    }

    componentWillMount() {
        fetch( 'http://localhost:8080/base/users/getUserById?id=' + this.props.match.params.id, {
            // mode: 'no-cors',
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then( (response) => response.json())
            .then(body => {
                console.log(body);
                this.setState( {user: body} )
            });
    }

    handleLogOut = () => {
        console.log("logout");
        window.location = "http://localhost:3000/login";
    };

    handleGoToReservations = () => {
        window.location = "http://localhost:3000/user/reservations/" + this.state.user.userId;
    }

    render(){
        let username;
        this.state.user ? username = this.state.user.userName : username = null;

        return(
            <Box direction={"row"}>
                <Box>
                    <Menu icon={<User />} items={[ {label: "logout", onClick: this.handleLogOut}]} />
                </Box>
                <Box direction={"column"}>
                    <Box id={"welcome"}>
                        <Text alignSelf={"center"} color={"neutral-3"} size={"xxlarge"}> Welcome {username}! </Text>
                    </Box>
                    <Box id={"go-to-reservations"}>
                        <Button id="reservation-button" onClick={this.handleGoToReservations}> Go To Reservations </Button>
                    </Box>
                </Box>
            </Box>
        );
    }
}