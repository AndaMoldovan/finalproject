import React from 'react';
import { useState } from 'react';
import {Box, Button, FormField, Layer, Text, TextInput} from 'grommet';
import { Close } from 'grommet-icons/icons/Close';

function handleAddSport(name, description, people, equipment){
    let username = "admin";
    let password = "11";
    let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
    fetch( "http://localhost:8080/base/sport/saveSport", {
        // mode: 'no-cors',
        method: 'POST',
        headers: {
            'Authorization': bearer,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "sportId" : 0,
            "name" : name,
            "description": description,
            "noPeople" : people,
            "equipment": equipment
        })
    }).then( (response) => response.json()).then( body => console.log(body))
}

function handleUpdateSport(name, description, people, equipment){
    let username = "admin";
    let password = "11";
    let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
    fetch( "http://localhost:8080/base/sport/updateSport", {
        // mode: 'no-cors',
        method: 'PUT',
        headers: {
            'Authorization': bearer,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "sportId" : -1,
            "name" : name,
            "description": description,
            "noPeople" : people,
            "equipment": equipment
        })
    }).then( (response) => response.json()).then( body => console.log(body))
}

function handleDeleteSport(name){
    let username = "admin";
    let password = "11";
    let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
    fetch( "http://localhost:8080/base/sport/deleteSportByName?name=" + name, {
        // mode: 'no-cors',
        method: 'DELETE',
        headers: {
            'Authorization': bearer,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    }).then( (response) => response.json()).then( body => console.log(body))
}

export function Slider ( { onClose, action }){
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [people, setPeople] = useState('');
    const [equipment, setEquipment] = useState('');

    let title;
    let event;
    if(action === 'add'){
        event = <Button onClick={() => handleAddSport(name, description, people, equipment)}> Add </Button>;
        title = 'Add';
    }else if(action === 'update'){
        event = <Button onClick={() => handleUpdateSport(name, description, people, equipment)}> Add </Button>;
        title = 'Update';
    }else if(action === 'delete'){
        event = <Button onClick={() => handleDeleteSport(name)}> Add </Button>;
        title = 'Delete';
    }

    return(
        <Layer position={"left"} full={"vertical"} modal={false} plain={"true"} onEsc={onClose}>
            <Box background={"brand"} fill={"vertical"}>
                <Box pad={{horizontal: 'medium', vertical: 'small'}}>
                    <Text size={"large"}> {title} a Sport </Text>
                </Box>
                <Box>
                    <FormField label="Name">
                        <TextInput type="text" value = {name} onChange={ (event) => setName(event.target.value)}/>
                    </FormField>
                    {(action !== 'delete') && <FormField label="Description">
                        <TextInput type="text" value = {description} onChange={ (event) => setDescription(event.target.value)}/>
                    </FormField>}
                    {(action !== 'delete') && <FormField label="People">
                        <TextInput type="text" value = {people} onChange={ (event) => setPeople(event.target.value)}/>
                    </FormField>}
                    {(action !== 'delete') && <FormField label="Equipment">
                        <TextInput type="text" value = {equipment} onChange={ (event) => setEquipment(event.target.value)}/>
                    </FormField>}
                    {event}
                    <Button onClick={onClose}> Done </Button>
                </Box>
            </Box>
        </Layer>
    );
}