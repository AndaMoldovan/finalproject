import React from 'react';
import { useState } from 'react'
import { Box, Button } from 'grommet';
import './style/_all.css';
import {Slider} from "../sports/Slider";

const buttonStyle = {
    "side" : "all",
    "color" : "red",
    "size" : "small"
};

export default function SportOptions(){
    const [option, setOption] = useState('none');

    return(
        <Box direction={"row"}>
            <Box border={buttonStyle} id="add-sport">
                <Button onClick={() => setOption('add')}> Add Sport </Button>
            </Box>
            <Box border={buttonStyle}  id="update-sport">
                <Button onClick={() => setOption('update')}> Update Sport </Button>
            </Box>
            <Box border={buttonStyle}  id="delete-sport">
                <Button onClick={() => setOption('delete')}> Delete SPort </Button>
            </Box>
            {(option === 'add') && <Slider onClose={() => setOption('none')} action={"add"}/>}
            {(option === 'update') && <Slider onClose={() => setOption('none')} action={"update"}/>}
            {(option === 'delete') && <Slider onClose={() => setOption('none')} action={"delete"}/>}
        </Box>
    );
}