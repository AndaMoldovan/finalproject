import React from 'react';
import {Box, Button} from 'grommet';
import { FormPreviousLink } from 'grommet-icons/icons/FormPreviousLink';
import Sports from './Sports';
import SportOptions from './SportOptions';

function handleGoBack(){
    window.location = "http://localhost:3000/admin";
}

export default function MainSport(){
    return(
        <Box direction={"column"}>
            <Button id="sports-go-back" icon={<FormPreviousLink />} onClick={() => handleGoBack()} />
            <Box>
                <Sports />
            </Box>
            <Box>
                <SportOptions />
            </Box>
        </Box>
    );
}