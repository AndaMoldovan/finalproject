import React from 'react';
import { Box } from 'grommet';
import {
    Table,
    TableBody,
    TableCell,
    TableHeader,
    TableRow,
    Text
} from 'grommet';

const COLUMNS = [
    {
        property: 'name',
        label: 'Name',
        format: datum => <Text weight={"bold"}>{datum.name}</Text>
    },
    {
        property: 'description',
        label: 'Description'
    },
    {
        property: 'noPeople',
        label: 'No. People'
    },
    {
        property: 'equipment',
        label: 'Equipment'
    }
];

export default class Sports extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            sports: []
        }
    }

    componentWillMount(){
        let username = 'admin';
        let password = '11';
        let bearer = 'Basic ' + Buffer(username + ':' + password).toString('base64');
        fetch( "http://localhost:8080/base/sport/getAllSports", {
            // mode: 'no-cors',
            method: 'GET',
            headers: {
                'Authorization': bearer,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then( (response) => response.json())
            .then( (body) => {
                this.setState({sports: body});
            });
    }

    render(){
        let sp = [];
        this.state.sports ? sp = this.state.sports : sp = [];

        let table = <Table caption={"Sports Table"}>
            <TableHeader>
                <TableRow>
                    {COLUMNS.map( c => (
                        <TableCell key={c.property} scope={"col"}>
                            <Text>{c.label}</Text>
                        </TableCell>
                    ))}
                </TableRow>
            </TableHeader>
            <TableBody>
                {sp.map(datum => (
                    <TableRow key={datum.id}>
                        {COLUMNS.map(c => (
                            <TableCell key={c.property}>
                                <Text>{datum[c.property]}</Text>
                            </TableCell>
                        ))}
                    </TableRow>
                ))}
            </TableBody>
        </Table>;

        return(
            <Box align={"centre"} pad={"large"}>
                {table}
            </Box>
        );
    }
}