import React from 'react'
import { FormField, TextInput, Box, Button } from "grommet";

export default class Register extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            email: '',
            password: ''
        };
    };

    handleButtonClick = () => {
        console.log(this.state.name);
        console.log(this.state.email);
        console.log(this.state.password);
        
        fetch( 'http://localhost:8080/base/users/saveUser', {
            // mode: 'no-cors',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "email": this.state.email,
                "password": this.state.password,
                "roleDTO": {
                    "role": "USER",
                    "roleId": 2
                },
                "userId": 0,
                "userName": this.state.name
            })
        }).then( (response) => console.log("Result: " + response.status))
            .then( (data) => console.log("Data: " + data));

        this.setState({name: '', email: '', password: ''})
    };

    render(){
        return(
            <Box>
                <FormField label="Name">
                    <TextInput type="text" value = {this.state.name} onChange={event => this.setState({name: event.target.value})}/>
                </FormField>
                <FormField label="Email">
                    <TextInput type="email" value = {this.state.email} onChange={event => this.setState({email: event.target.value})}/>
                </FormField>
                <FormField label="Password">
                    <TextInput type="password" value={this.state.password} onChange={event => this.setState({password: event.target.value})}/>
                </FormField>
                <Button alignSelf={"center"} margin="medium" onClick={this.handleButtonClick}> Register </Button>
            </Box>
        );

    }
}