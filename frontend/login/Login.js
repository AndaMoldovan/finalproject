import React from 'react';
import { FormField, TextInput, Box, Button } from "grommet";

export default class Login extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            email: '',
            password: ''
        }
    }

    handleButtonClick = () => {
        console.log("email: " + this.state.email + "\n password: " + this.state.password);

        fetch( 'http://localhost:8080/base/users/getUserByEmail?email=' + this.state.email, {
            // mode: 'no-cors',
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then( (response) => response.json())
            .then(body => {
                console.log(body);
                (body.password === this.state.password)
                    ?  (body.roleDTO.role === 'USER') ? window.location = "http://localhost:3000/user/" + body.userId : window.location = "http://localhost:3000/admin"
                    : console.log("Failed to Log in");
            });
    };

    render(){
        return(
            <Box>
                <FormField label="Email">
                    <TextInput type="email" value = {this.state.value} onChange={event => this.setState({email: event.target.value})}/>
                </FormField>
                <FormField label="Password">
                    <TextInput type="password" value={this.state.password} onChange={event => this.setState({password: event.target.value})}/>
                </FormField>
                <Button alignSelf={"center"} margin="medium" onClick={this.handleButtonClick}> Login </Button>
            </Box>
        );
    }
}