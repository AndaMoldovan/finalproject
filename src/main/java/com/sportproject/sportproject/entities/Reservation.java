package com.sportproject.sportproject.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.omg.CORBA.TIMEOUT;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "reservation")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long reservationId;

    @Column(name = "start_date")
    private Timestamp startDate;

    @Column(name = "end_date")
    private Timestamp endDate;

    @Column(name = "no_people")
    private int noPeople;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User userIdentifier;

    @ManyToOne
    @JoinColumn(name="facility_sport_id", nullable = false)
    private SportFacility sportFacilityIdentifier;

    public Reservation() {}

    public Reservation(Timestamp startDate, Timestamp endDate, int noPeople, User userIdentifier, SportFacility sportFacilityIdentifier) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.noPeople = noPeople;
        this.userIdentifier = userIdentifier;
        this.sportFacilityIdentifier = sportFacilityIdentifier;
    }

    public Long getReservationId() {
        return reservationId;
    }

    public void setReservationId(Long reservationId) {
        this.reservationId = reservationId;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public int getNoPeople() {
        return noPeople;
    }

    public void setNoPeople(int noPeople) {
        this.noPeople = noPeople;
    }

    public User getUserIdentifier() {
        return userIdentifier;
    }

    public void setUserIdentifier(User userIdentifier) {
        this.userIdentifier = userIdentifier;
    }

    public SportFacility getSportFacilityIdentifier() {
        return sportFacilityIdentifier;
    }

    public void setSportFacilityIdentifier(SportFacility sportFacilityIdentifier) {
        this.sportFacilityIdentifier = sportFacilityIdentifier;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "reservationId=" + reservationId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", noPeople=" + noPeople +
                ", userIdentifier=" + userIdentifier +
                ", sportFacilityIdentifier=" + sportFacilityIdentifier +
                '}';
    }
}
