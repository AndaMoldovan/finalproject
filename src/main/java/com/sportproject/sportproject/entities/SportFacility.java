package com.sportproject.sportproject.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "sport_facility")
public class SportFacility {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name="facility_id", nullable = false)
    private Facility facilityIdentifier;

    @ManyToOne
    @JoinColumn(name = "sport_id", nullable = false)
    private Sport sportIdentifier;

    @OneToMany(mappedBy = "sportFacilityIdentifier")
    private Set<Reservation> reservations = new HashSet<>();

    public SportFacility() {}

    public SportFacility(Sport sportIdentifier, Facility facilityIdentifier) {
        this.facilityIdentifier = facilityIdentifier;
        this.sportIdentifier = sportIdentifier;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Facility getFacilityIdentifier() {
        return facilityIdentifier;
    }

    public void setFacilityIdentifier(Facility facilityIdentifier) {
        this.facilityIdentifier = facilityIdentifier;
    }

    public Sport getSportIdentifier() {
        return sportIdentifier;
    }

    public void setSportIdentifier(Sport sportIdentifier) {
        this.sportIdentifier = sportIdentifier;
    }

    @Override
    public String toString() {
        return "SportFacility{" +
                "id=" + id +
                ", facilityIdentifier=" + facilityIdentifier +
                ", sportIdentifier=" + sportIdentifier +
                '}';
    }
}
