package com.sportproject.sportproject.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "sport")
public class Sport {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long sportId;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "noPeople")
    private int noPeople;

    @Column(name = "equipment")
    private String equipment;

    @OneToMany(mappedBy = "sportIdentifier", fetch = FetchType.LAZY)
    private Set<SportFacility> sportFacilites = new HashSet<>();

    public Sport() {}

    public Sport(String name, String description, int noPeople, String equipment) {
        this.name = name;
        this.description = description;
        this.noPeople = noPeople;
        this.equipment = equipment;
    }

    public Long getSportId() {
        return sportId;
    }

    public void setSportId(Long sportId) {
        this.sportId = sportId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNoPeople() {
        return noPeople;
    }

    public void setNoPeople(int noPeople) {
        this.noPeople = noPeople;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    @Override
    public String toString() {
        return "Sport{" +
                "sportId=" + sportId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", noPeople=" + noPeople +
                ", equipment='" + equipment + '\'' +
                '}';
    }
}
