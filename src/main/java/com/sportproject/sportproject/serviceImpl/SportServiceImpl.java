package com.sportproject.sportproject.serviceImpl;

import com.sportproject.sportproject.dao.SportDAO;
import com.sportproject.sportproject.entities.Sport;
import com.sportproject.sportproject.service.SportService;
import com.sportproject.sportproject.service.dto.SportDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SportServiceImpl implements SportService {

    private final SportDAO sportDAO;

    @Autowired
    public SportServiceImpl(SportDAO sportDAO){
        this.sportDAO = sportDAO;
    }

    @Override
    public List<SportDTO> getAllSports() {
        List<Sport> sports = sportDAO.findAll();
        List<SportDTO> result = new ArrayList<SportDTO>();

        for(Sport sport : sports){
            result.add(transform(sport));
        }
        return result;
    }

    @Override
    public SportDTO getSportByIn(Long id) {
        return transform(sportDAO.getOne(id));
    }

    @Override
    public SportDTO saveSport(SportDTO sportDTO) {
        Sport sport = transform(sportDTO);
        try{
            SportDTO newSport = transform(sportDAO.save(sport));
            return newSport;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SportDTO updateSport(Long id, SportDTO sportDTO) {
        Sport sport;
        if(id == -1){
            sport = sportDAO.findByName(sportDTO.getName());
            sport.setDescription(sportDTO.getDescription());
            sport.setNoPeople(sportDTO.getNoPeople());
            sport.setEquipment(sportDTO.getEquipment());
        }else {
            sport = transform(sportDTO);
            sport.setSportId(id);
        }
        try{
            SportDTO newSport = transform(sportDAO.save(sport));
            return newSport;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteSport(Long id) {
        try{
            Sport sport = sportDAO.getOne(id);
            sportDAO.delete(sport);
            return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteSportByName(String name) {
        try{
            Sport sport = sportDAO.findByName(name);
            sportDAO.delete(sport);
            return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    private SportDTO transform(Sport sport){
        return new SportDTO(sport.getSportId(), sport.getName(), sport.getDescription(), sport.getNoPeople(), sport.getEquipment());
    }

    private Sport transform(SportDTO sportDTO){
        return new Sport(sportDTO.getName(), sportDTO.getDescription(), sportDTO.getNoPeople(), sportDTO.getEquipment());
    }
}
