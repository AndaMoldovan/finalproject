package com.sportproject.sportproject.serviceImpl;

import com.sportproject.sportproject.dao.UserRoleDAO;
import com.sportproject.sportproject.entities.UserRole;
import com.sportproject.sportproject.service.UserRoleService;
import com.sportproject.sportproject.service.dto.UserRoleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserRoleServiceImpl implements UserRoleService {

    private final UserRoleDAO userRoleDAO;

    @Autowired
    public UserRoleServiceImpl(UserRoleDAO userRoleDAO){
        this.userRoleDAO = userRoleDAO;
    }

    @Override
    public List<UserRoleDTO> getAllUserRoles() {
        List<UserRoleDTO> rolesDTO = new ArrayList<>();

        try{
            List<UserRole> roles = new ArrayList<>();
            roles = userRoleDAO.findAll();

            for(UserRole role : roles){
                UserRoleDTO roleDTO = new UserRoleDTO(role.getRoleId(), role.getRole());
                rolesDTO.add(roleDTO);
            }
        }catch(Exception e) {
            e.printStackTrace();
        }

        return rolesDTO;
    }

    @Override
    public UserRoleDTO getRoleById(Long id) {
        UserRoleDTO userRoleDTO = new UserRoleDTO();
        try{
            UserRole userRole = userRoleDAO.getOne(id);
            userRoleDTO.setRoleId(userRole.getRoleId());
            userRoleDTO.setRole(userRole.getRole());
        }catch(Exception e){
            e.printStackTrace();
        }

        return userRoleDTO;
    }
}
