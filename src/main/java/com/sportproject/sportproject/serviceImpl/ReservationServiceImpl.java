package com.sportproject.sportproject.serviceImpl;

import com.sportproject.sportproject.dao.ReservationDAO;
import com.sportproject.sportproject.dao.SportFacilityDAO;
import com.sportproject.sportproject.dao.UserDAO;
import com.sportproject.sportproject.entities.*;
import com.sportproject.sportproject.service.ReservationService;
import com.sportproject.sportproject.service.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReservationServiceImpl implements ReservationService {

    private final ReservationDAO reservationDAO;
    private final UserDAO userDAO;
    private final SportFacilityDAO sportFacilityDAO;

    @Autowired
    public ReservationServiceImpl(ReservationDAO reservationDAO, UserDAO userDAO,
                                  SportFacilityDAO sportFacilityDAO){
        this.reservationDAO = reservationDAO;
        this.userDAO = userDAO;
        this.sportFacilityDAO = sportFacilityDAO;
    }

    @Override
    public List<ReservationDTO> getAllReservation() {
        List<Reservation> reservations = reservationDAO.findAll();
        List<ReservationDTO> results = new ArrayList<ReservationDTO>();

        for(Reservation reservation : reservations){
            results.add(transform(reservation));
        }
        return results;
    }

    @Override
    public ReservationDTO getReservationById(Long id) {
        try{
            Reservation reservation = reservationDAO.getOne(id);
            return transform(reservation);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ReservationDTO saveReservation(ReservationDTO reservationDTO, Long userId, Long sportFacilityId) {
        try{
            User user = userDAO.getOne(userId);
            SportFacility sportFacility = sportFacilityDAO.getOne(sportFacilityId);

            Reservation reservation = new Reservation();
            reservation.setStartDate(reservationDTO.getStartDate());
            reservation.setEndDate(reservation.getEndDate());
            reservation.setNoPeople(reservationDTO.getNoPeople());
            reservation.setUserIdentifier(user);
            reservation.setSportFacilityIdentifier(sportFacility);

            Reservation newReservation = reservationDAO.save(reservation);
            return transform(newReservation);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ReservationDTO updateReservation(long id, ReservationDTO reservationDTO, Long userId, Long sportFacilityId) {
        try{
            User user = userDAO.getOne(userId);
            SportFacility sportFacility = sportFacilityDAO.getOne(sportFacilityId);

            Reservation reservation = new Reservation();
            reservation.setReservationId(id);
            reservation.setStartDate(reservationDTO.getStartDate());
            reservation.setEndDate(reservation.getEndDate());
            reservation.setNoPeople(reservationDTO.getNoPeople());
            reservation.setUserIdentifier(user);
            reservation.setSportFacilityIdentifier(sportFacility);

            Reservation newReservation = reservationDAO.save(reservation);
            return transform(newReservation);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteReservationDTO(Long id) {
        try{
            reservationDAO.deleteById(id);
            return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    private ReservationDTO transform(Reservation reservation){
        User user = reservation.getUserIdentifier();
        SportFacility sportFacility = reservation.getSportFacilityIdentifier();

        UserRole userRole = user.getRole();
        UserRoleDTO userRoleDTO = new UserRoleDTO(userRole.getRoleId(), userRole.getRole());
        UserDTO userDTO = new UserDTO(user.getUserId(), user.getUserName(), user.getEmail(), user.getPassword(), userRoleDTO);

        Sport sport = sportFacility.getSportIdentifier();
        SportDTO sportDTO = new SportDTO(sport.getSportId(), sport.getName(), sport.getDescription(), sport.getNoPeople(), sport.getEquipment());
        Facility facility = sportFacility.getFacilityIdentifier();
        FacilityDTO facilityDTO = new FacilityDTO(facility.getFacilityId(), facility.getName(), facility.getDescription(), facility.getAddress());
        SportFacilityDTO sportFacilityDTO = new SportFacilityDTO(sportFacility.getId(), sportDTO, facilityDTO);

        return new ReservationDTO(reservation.getReservationId(), reservation.getStartDate(), reservation.getEndDate(), reservation.getNoPeople(), userDTO, sportFacilityDTO);
    }
}
