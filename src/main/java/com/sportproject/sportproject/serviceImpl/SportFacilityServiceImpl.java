package com.sportproject.sportproject.serviceImpl;

import com.sportproject.sportproject.dao.FacilityDAO;
import com.sportproject.sportproject.dao.SportDAO;
import com.sportproject.sportproject.dao.SportFacilityDAO;
import com.sportproject.sportproject.entities.Facility;
import com.sportproject.sportproject.entities.Sport;
import com.sportproject.sportproject.entities.SportFacility;
import com.sportproject.sportproject.service.SportFacilityService;
import com.sportproject.sportproject.service.dto.FacilityDTO;
import com.sportproject.sportproject.service.dto.SportDTO;
import com.sportproject.sportproject.service.dto.SportFacilityDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SportFacilityServiceImpl implements SportFacilityService {

    private final SportFacilityDAO sportFacilityDAO;
    private final SportDAO sportDAO;
    private final FacilityDAO facilityDAO;

    @Autowired
    public SportFacilityServiceImpl(SportFacilityDAO sportFacilityDAO, SportDAO sportDAO, FacilityDAO facilityDAO){
        this.sportFacilityDAO = sportFacilityDAO;
        this.sportDAO = sportDAO;
        this.facilityDAO = facilityDAO;
    }

    @Override
    public List<SportFacilityDTO> getAllSportFacilities() {
        List<SportFacilityDTO> result = new ArrayList<>();
        try{
            List<SportFacility> list = sportFacilityDAO.findAll();
//            list.stream().map( el -> result.add(tracsform(el)));
            for(SportFacility sp : list){
                result.add(tracsform(sp));
                System.out.println(sp.toString());
            }
            return result;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SportFacilityDTO getSportFacilityById(Long id) {
        try{
            return tracsform(sportFacilityDAO.getOne(id));
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SportFacilityDTO saveSportFacility(Long sportId, Long facilityId) {
        try{
            Sport sport = sportDAO.getOne(sportId);
            Facility facility = facilityDAO.getOne(facilityId);

            SportFacility sportFacility = new SportFacility(sport, facility);
            SportFacility newSportFacility = sportFacilityDAO.save(sportFacility);

            return tracsform(newSportFacility);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SportFacilityDTO updateSportFacility(Long id, Long sportId, Long facilityId) {
        try{
            Sport sport = sportDAO.getOne(sportId);
            Facility facility = facilityDAO.getOne(facilityId);

            SportFacility sportFacility = new SportFacility(sport, facility);
            sportFacility.setId(id);
            SportFacility newSportFacility = sportFacilityDAO.save(sportFacility);

            return tracsform(newSportFacility);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public boolean deleteSportFacility(Long id) {
        try{
            sportDAO.deleteById(id);
            return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    private SportFacilityDTO tracsform(SportFacility sportFacility){
        Sport sport = sportFacility.getSportIdentifier();
        Facility facility = sportFacility.getFacilityIdentifier();

        SportDTO sportDTO = new SportDTO(sport.getSportId(), sport.getName(), sport.getDescription(), sport.getNoPeople(), sport.getEquipment());
        FacilityDTO facilityDTO = new FacilityDTO(facility.getFacilityId(), facility.getName(), facility.getDescription(), facility.getAddress());

        return new SportFacilityDTO(sportFacility.getId(), sportDTO, facilityDTO);
    }

    private SportFacility transform(SportFacilityDTO sportFacilityDTO){
        SportDTO sportDTO = sportFacilityDTO.getSportDTO();
        FacilityDTO facilityDTO = sportFacilityDTO.getFacilityDTO();

        Sport sport = new Sport(sportDTO.getName(), sportDTO.getDescription(), sportDTO.getNoPeople(), sportDTO.getEquipment());
        sport.setSportId(sportDTO.getSportId());

        Facility facility = new Facility(facilityDTO.getName(), facilityDTO.getDescription(), facilityDTO.getAddress());
        facility.setFacilityId(facilityDTO.getId());

        return new SportFacility(sport, facility);
    }
}
