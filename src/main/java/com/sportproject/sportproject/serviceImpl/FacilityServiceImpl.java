package com.sportproject.sportproject.serviceImpl;

import com.sportproject.sportproject.dao.FacilityDAO;
import com.sportproject.sportproject.entities.Facility;
import com.sportproject.sportproject.service.FacilityService;
import com.sportproject.sportproject.service.dto.FacilityDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FacilityServiceImpl implements FacilityService {

    private final FacilityDAO facilityDAO;

    public FacilityServiceImpl(FacilityDAO facilityDAO){
        this.facilityDAO = facilityDAO;
    }

    @Override
    public List<FacilityDTO> getAllFacilities() {
        List<Facility> facilities = facilityDAO.findAll();
        List<FacilityDTO> result = new ArrayList<FacilityDTO>();

        for(Facility facility : facilities){
            result.add(transform(facility));
        }
        return result;
    }

    @Override
    public FacilityDTO getFacilityById(Long id) {
        try{
            Facility facility = facilityDAO.getOne(id);
            return transform(facility);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FacilityDTO saveFacility(FacilityDTO facilityDTO) {
        try{
            Facility facility = facilityDAO.save(transform(facilityDTO));
            return transform(facility);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FacilityDTO updateFacility(Long id, FacilityDTO facilityDTO) {
        try{
            Facility facility = transform(facilityDTO);
            if(id == -1) {
                Facility fac = facilityDAO.findByAddress(facilityDTO.getAddress());

                facility.setFacilityId(fac.getFacilityId());
            }else {
                facility.setFacilityId(id);
            }

            return transform(facilityDAO.save(facility));
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteFacility(Long id) {
        try{
            facilityDAO.deleteById(id);
            return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteFacilityByAddress(String address) {
        try{
            Facility fac = facilityDAO.findByAddress(address);
            facilityDAO.delete(fac);
            return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    private FacilityDTO transform(Facility facility){
        return new FacilityDTO(facility.getFacilityId(), facility.getName(), facility.getDescription(), facility.getAddress());
    }

    private Facility transform(FacilityDTO facilityDTO){
        return new Facility(facilityDTO.getName(), facilityDTO.getDescription(), facilityDTO.getAddress());
    }
}
