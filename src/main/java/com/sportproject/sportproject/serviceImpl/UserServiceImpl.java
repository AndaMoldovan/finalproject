package com.sportproject.sportproject.serviceImpl;

import com.sportproject.sportproject.config.CustomUserDetails;
import com.sportproject.sportproject.dao.UserDAO;
import com.sportproject.sportproject.dao.UserRoleDAO;
import com.sportproject.sportproject.entities.User;
import com.sportproject.sportproject.entities.UserRole;
import com.sportproject.sportproject.service.UserService;
import com.sportproject.sportproject.service.dto.UserDTO;
import com.sportproject.sportproject.service.dto.UserRoleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserDAO userDAO;
    private final UserRoleDAO userRoleDAO;

    @Autowired
    public UserServiceImpl(UserDAO userDAO, UserRoleDAO userROleDAO){

        this.userDAO = userDAO;
        this.userRoleDAO = userROleDAO;
    }

    @Override
    public UserDTO getUserById(Long id) {
        User user = userDAO.getOne(id);
        UserRole userRole = user.getRole();

        UserRoleDTO userRoleDTO = transformUserRole(userRole);

        UserDTO userDTO = new UserDTO(id, user.getUserName(), user.getEmail(), user.getPassword(), userRoleDTO);
        return userDTO;
    }

    @Override
    public UserDTO getUserByEmail(String email) {
        try{
            Optional<User> user = userDAO.findByEmail(email);
            User newUser = user.map(CustomUserDetails::new).get();
            return transform(newUser);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public UserDTO saveUser(UserDTO userDTO, Long userRoleId) {
        UserRole userRole = userRoleDAO.getOne(userRoleId);

        User newUser = new User();
        newUser.setUserName(userDTO.getUserName());
        newUser.setEmail(userDTO.getEmail());
        newUser.setRole(userRole);
        newUser.setPassword(userDTO.getPassword());

        try{
//           if(userDAO.findByEmail(userDTO.getEmail()) == null){
               userDAO.save(newUser);
               return transform(newUser);
          // }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public UserDTO updateUser(Long id, UserDTO userDTO, Long userRoleID) {
        UserRole userRole = userRoleDAO.getOne(userRoleID);

        User user = userDAO.getOne(id);
        user.setUserName(userDTO.getUserName());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setRole(userRole);

        try{
            return transform(userDAO.save(user));
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteUser(Long id) {
        try{
            userDAO.deleteById(id);
            return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    private UserRoleDTO transformUserRole(UserRole userRole){
        UserRoleDTO userRoleDTO = new UserRoleDTO(userRole.getRoleId(), userRole.getRole());
        return userRoleDTO;
    }

    private UserRole transformUserRole(UserRoleDTO userRoleDTO){
        UserRole userRole = new UserRole(userRoleDTO.getRole());
        userRole.setRoleId(userRoleDTO.getRoleId());
        return userRole;
    }

    private UserDTO transform(User user){
        UserRoleDTO userRoleDTO = transformUserRole(user.getRole());
        return new UserDTO(user.getUserId(), user.getUserName(), user.getEmail(), user.getPassword(), userRoleDTO);
    }
}
