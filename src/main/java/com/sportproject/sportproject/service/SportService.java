package com.sportproject.sportproject.service;

import com.sportproject.sportproject.service.dto.SportDTO;
import java.util.List;

public interface SportService {

    List<SportDTO> getAllSports();

    SportDTO getSportByIn(Long id);

    SportDTO saveSport(SportDTO sportDTO);

    SportDTO updateSport(Long id, SportDTO sportDTO);

    boolean deleteSport(Long id);

    boolean deleteSportByName(String name);
}
