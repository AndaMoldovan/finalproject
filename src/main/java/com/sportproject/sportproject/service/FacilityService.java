package com.sportproject.sportproject.service;

import com.sportproject.sportproject.service.dto.FacilityDTO;

import java.util.List;

public interface FacilityService {

    List<FacilityDTO> getAllFacilities();

    FacilityDTO getFacilityById(Long id);

    FacilityDTO saveFacility(FacilityDTO facilityDTO);

    FacilityDTO updateFacility(Long id, FacilityDTO facilityDTO);

    boolean deleteFacility(Long id);

    boolean deleteFacilityByAddress(String address);
}
