package com.sportproject.sportproject.service;

import com.sportproject.sportproject.entities.SportFacility;
import com.sportproject.sportproject.service.dto.SportFacilityDTO;

import java.util.List;

public interface SportFacilityService {

    List<SportFacilityDTO> getAllSportFacilities();

    SportFacilityDTO getSportFacilityById(Long id);

    SportFacilityDTO saveSportFacility(Long sportId, Long facilityId);

    SportFacilityDTO updateSportFacility(Long id, Long sportId, Long facilityId);

    boolean deleteSportFacility(Long id);
}
