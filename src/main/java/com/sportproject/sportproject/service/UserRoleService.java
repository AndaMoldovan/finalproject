package com.sportproject.sportproject.service;

import com.sportproject.sportproject.entities.UserRole;
import com.sportproject.sportproject.service.dto.UserRoleDTO;

import java.util.List;

public interface UserRoleService {

    List<UserRoleDTO> getAllUserRoles();

    UserRoleDTO getRoleById(Long id);

}
