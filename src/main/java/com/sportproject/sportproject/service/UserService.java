package com.sportproject.sportproject.service;

import com.sportproject.sportproject.service.dto.UserDTO;

public interface UserService {

    UserDTO getUserById(Long id);

    UserDTO getUserByEmail(String email);

    UserDTO saveUser(UserDTO userDTO, Long userRoleId);

    UserDTO updateUser(Long id, UserDTO userDTO, Long userRoleId);

    boolean deleteUser(Long id);
}
