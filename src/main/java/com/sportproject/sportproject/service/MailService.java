package com.sportproject.sportproject.service;

public interface MailService {

    void sendMail(String username, String password, String to, String subject, String content);

    void sendMail(String to, String subject, String content);
}
