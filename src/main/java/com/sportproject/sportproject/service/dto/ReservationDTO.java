package com.sportproject.sportproject.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.sql.Timestamp;

public class ReservationDTO implements Serializable {

    private Long id;

    @JsonFormat( shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp startDate;

    @JsonFormat( shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp endDate;
    private int noPeople;
    private UserDTO userDTO;
    private SportFacilityDTO sportFacilityDTO;

    public ReservationDTO() {}

    public ReservationDTO(Long id, Timestamp startDate, Timestamp endDate, int noPeople, UserDTO userDTO, SportFacilityDTO sportFacilityDTO) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.noPeople = noPeople;
        this.userDTO = userDTO;
        this.sportFacilityDTO = sportFacilityDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public int getNoPeople() {
        return noPeople;
    }

    public void setNoPeople(int noPeople) {
        this.noPeople = noPeople;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public SportFacilityDTO getSportFacilityDTO() {
        return sportFacilityDTO;
    }

    public void setSportFacilityDTO(SportFacilityDTO sportFacilityDTO) {
        this.sportFacilityDTO = sportFacilityDTO;
    }

    @Override
    public String toString() {
        return "ReservationDTO{" +
                "id=" + id +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", noPeople=" + noPeople +
                ", userDTO=" + userDTO +
                ", sportFacilityDTO=" + sportFacilityDTO +
                '}';
    }
}
