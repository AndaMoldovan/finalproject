package com.sportproject.sportproject.service.dto;

import java.io.Serializable;

public class SportDTO implements Serializable {

    private Long sportId;
    private String name;
    private String description;
    private int noPeople;
    private String equipment;

    public SportDTO() {}

    public SportDTO(Long sportId, String name, String description, int noPeople, String equipment) {
        this.sportId = sportId;
        this.name = name;
        this.description = description;
        this.noPeople = noPeople;
        this.equipment = equipment;
    }

    public Long getSportId() {
        return sportId;
    }

    public void setSportId(Long sportId) {
        this.sportId = sportId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNoPeople() {
        return noPeople;
    }

    public void setNoPeople(int noPeople) {
        this.noPeople = noPeople;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    @Override
    public String toString() {
        return "SportDTO{" +
                "sportId=" + sportId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", noPeople=" + noPeople +
                ", equipment='" + equipment + '\'' +
                '}';
    }
}
