package com.sportproject.sportproject.service.dto;

public class SportFacilityDTO {

    private Long id;
    private SportDTO sportDTO;
    private FacilityDTO facilityDTO;

    public SportFacilityDTO() {}

    public SportFacilityDTO(Long id, SportDTO sportDTO, FacilityDTO facilityDTO) {
        this.id = id;
        this.sportDTO = sportDTO;
        this.facilityDTO = facilityDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SportDTO getSportDTO() {
        return sportDTO;
    }

    public void setSportDTO(SportDTO sportDTO) {
        this.sportDTO = sportDTO;
    }

    public FacilityDTO getFacilityDTO() {
        return facilityDTO;
    }

    public void setFacilityDTO(FacilityDTO facilityDTO) {
        this.facilityDTO = facilityDTO;
    }

    @Override
    public String toString() {
        return "SportFacilityDTO{" +
                "id=" + id +
                ", sportDTO=" + sportDTO +
                ", facilityDTO=" + facilityDTO +
                '}';
    }
}
