package com.sportproject.sportproject.service.dto;

import java.io.Serializable;

public class UserDTO implements Serializable {

    private Long userId;
    private String userName;
    private String email;
    private String password;
    private UserRoleDTO roleDTO;

    public UserDTO() {}

    public UserDTO(Long userId, String userName, String email, String password, UserRoleDTO roleDTO) {
        this.userId = userId;
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.roleDTO = roleDTO;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRoleDTO getRoleDTO() {
        return roleDTO;
    }

    public void setRoleDTO(UserRoleDTO roleDTO) {
        this.roleDTO = roleDTO;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", roleDTO=" + roleDTO +
                '}';
    }
}
