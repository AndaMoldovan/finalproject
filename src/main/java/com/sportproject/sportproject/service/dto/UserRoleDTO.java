package com.sportproject.sportproject.service.dto;

import java.io.Serializable;

public class UserRoleDTO implements Serializable {

    private Long roleId;
    private String role;

    public UserRoleDTO() {}

    public UserRoleDTO(Long roleId, String role) {
        this.roleId = roleId;
        this.role = role;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserRoleDTO{" +
                "roleId=" + roleId +
                ", role='" + role + '\'' +
                '}';
    }
}