package com.sportproject.sportproject.service;

import com.sportproject.sportproject.service.dto.ReservationDTO;

import java.util.List;

public interface ReservationService {

    List<ReservationDTO> getAllReservation();

    ReservationDTO getReservationById(Long id);

    ReservationDTO saveReservation(ReservationDTO reservationDTO, Long userId, Long sportFacilityId);

    ReservationDTO updateReservation(long id, ReservationDTO reservationDTO, Long userId, Long sportFacilityId);

    boolean deleteReservationDTO(Long id);
}
