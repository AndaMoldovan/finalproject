package com.sportproject.sportproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SportprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(SportprojectApplication.class, args);
    }
}
