package com.sportproject.sportproject.controllers;

import com.sportproject.sportproject.service.UserRoleService;
import com.sportproject.sportproject.service.dto.UserRoleDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/base/userRoles")
public class UserRoleController {

    private final UserRoleService userRoleService;

    @Autowired
    public UserRoleController(UserRoleService userRoleService){
        this.userRoleService = userRoleService;
    }

    @GetMapping("/getAllUsers")
    @ResponseBody
    public List<UserRoleDTO> getAllUserRoles(){

        return userRoleService.getAllUserRoles();
    }

    @GetMapping("/getUserById")
    @ResponseBody
    public UserRoleDTO getUserRoleById(@RequestParam("id") Long id){
        return userRoleService.getRoleById(id);
    }

}
