package com.sportproject.sportproject.controllers;

import com.sportproject.sportproject.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/base/mail")
public class MailController {

    private final MailService mailService;

    @Autowired
    public MailController(MailService mailService){
        this.mailService = mailService;
    }

    @GetMapping("/sendMail")
    @ResponseBody
    public boolean sentMail(@RequestParam("to") String to,
                            @RequestParam("subject") String subject,
                            @RequestParam("content") String content){
        try{
            mailService.sendMail(to, subject, content);
            return true;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
