package com.sportproject.sportproject.controllers;

import com.sportproject.sportproject.service.SportFacilityService;
import com.sportproject.sportproject.service.dto.SportFacilityDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/base/sportFacility")
public class SportFacilityController {

    private final SportFacilityService sportFacilityService;

    @Autowired
    public SportFacilityController(SportFacilityService sportFacilityService){
        this.sportFacilityService = sportFacilityService;
    }

    @GetMapping("/getAllSportFacilities")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    @ResponseBody
    public List<SportFacilityDTO> getAllSportFacilities(){
        return sportFacilityService.getAllSportFacilities();
    }

    @GetMapping("/getFacilityById")
    @ResponseBody
    public SportFacilityDTO getFacilityById(@RequestParam("id") Long id){
        return sportFacilityService.getSportFacilityById(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    @PostMapping("/saveFacility")
    @ResponseBody
    public SportFacilityDTO saveSportFacility(@RequestParam("sportId") Long sportId, @RequestParam("facilityId") long facilityId){
        return sportFacilityService.saveSportFacility(sportId, facilityId);
    }

    @PutMapping("/updateSportFacility")
    @ResponseBody
    public SportFacilityDTO updateSportFacility(@RequestParam("id") Long id, @RequestParam("sportId") Long sportId,
                                                @RequestParam("facilityId") long facilityId){
        return sportFacilityService.updateSportFacility(id, sportId, facilityId);
    }

    @DeleteMapping("/deleteSportFacility")
    @ResponseBody
    public boolean deleteSportFacility(@RequestParam("id") Long id){
        return sportFacilityService.deleteSportFacility(id);
    }
}
