package com.sportproject.sportproject.controllers;

import com.sportproject.sportproject.service.FacilityService;
import com.sportproject.sportproject.service.dto.FacilityDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.QueryParam;
import java.util.List;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/base/facilities")
public class FacilityController {

    private final FacilityService facilityService;

    @Autowired
    public FacilityController(FacilityService facilityService){
        this.facilityService = facilityService;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    @GetMapping("/getAllFacilities")
    @ResponseBody
    public List<FacilityDTO> getAllFacilities(){
        return facilityService.getAllFacilities();
    }


    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    @GetMapping("/getFacilityById")
    @ResponseBody
    public FacilityDTO getFacilityById(@RequestParam("id") Long id){
        return facilityService.getFacilityById(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/saveFacility")
    @ResponseBody
    public FacilityDTO saveFacility(@RequestBody FacilityDTO facilityDTO){

        facilityDTO.setId(null);
        return facilityService.saveFacility(facilityDTO);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/updateFacility")
    @ResponseBody
    public FacilityDTO updateFacility(@RequestBody FacilityDTO facilityDTO){

        return facilityService.updateFacility(facilityDTO.getId(), facilityDTO);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/deleteFacility")
    @ResponseBody
    public boolean deleteFacility(@RequestParam("id") Long id){
        return facilityService.deleteFacility(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/deleteFacilityByAddress")
    @ResponseBody
    public boolean deleteFacilityByAddress(@RequestParam("address") String address){
        return facilityService.deleteFacilityByAddress(address);
    }
}
