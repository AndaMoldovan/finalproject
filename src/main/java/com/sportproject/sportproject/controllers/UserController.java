package com.sportproject.sportproject.controllers;

import com.sportproject.sportproject.service.MailService;
import com.sportproject.sportproject.service.UserService;
import com.sportproject.sportproject.service.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/base/users")
public class UserController {
    private final MailService mailService;
    private final UserService userService;

    @Autowired
    public UserController(UserService userService, MailService mailService){
        this.userService = userService;
        this.mailService = mailService;
    }

    @GetMapping("/getUserById")
    @ResponseBody
    public UserDTO getUserById(@RequestParam("id") Long id){
        return userService.getUserById(id);
    }

    @GetMapping("/getUserByEmail")
    @ResponseBody
    public UserDTO getUserByEmail(@RequestParam("email") String email){
        return userService.getUserByEmail(email);
    }

    @PostMapping("/saveUser")
//    @CrossOrigin
    @ResponseBody
    public UserDTO saveUser(@RequestBody UserDTO userDTO){
        System.out.println("\n\n " + userDTO.toString() + " \n\n");
        mailService.sendMail(userDTO.getEmail(), "Registration", "You just registered to our website!");
        return userService.saveUser(userDTO, userDTO.getRoleDTO().getRoleId());
    }


    @PutMapping("/updateUser")
    @ResponseBody
    public UserDTO updateUser(@RequestParam("id") Long id, @RequestBody UserDTO userDTO, @RequestParam("roleid") Long roleId){

        return userService.updateUser(id, userDTO, roleId);
    }

    @DeleteMapping("/deleteUser")
    @ResponseBody
    public boolean deleteUser(@RequestParam("id") Long id){
        return userService.deleteUser(id);
    }
}
