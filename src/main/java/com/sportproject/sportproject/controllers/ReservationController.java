package com.sportproject.sportproject.controllers;

import com.sportproject.sportproject.service.ReservationService;
import com.sportproject.sportproject.service.dto.ReservationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/base/reservations")
public class ReservationController {

    private final ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationService reservationService){
        this.reservationService = reservationService;
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/getAllReservations")
    @ResponseBody
    public List<ReservationDTO> getAllReservations(){
        return reservationService.getAllReservation();
    }

    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN')")
    @GetMapping("/getReservationById")
    @ResponseBody
    public ReservationDTO getReservationById(@RequestParam("id") Long id){
        return reservationService.getReservationById(id);
    }

//    @PreAuthorize("hasAnyRole('ROLE_USER')")
//    @PostMapping(value = "/saveReservation", produces = "application/json; charset=utf-8")
//    @ResponseBody
//    public ReservationDTO saveReservation(@RequestParam("startDate")Timestamp startDate, @RequestParam("endDate") Timestamp endDate,
//                                          @RequestParam("noPeople") int noPeople, @RequestParam("userId") Long userId,
//                                          @RequestParam("sportFacilityId") Long sportFacilityId){
//        ReservationDTO reservationDTO = new ReservationDTO();
//        reservationDTO.setStartDate(startDate);
//        reservationDTO.setEndDate(endDate);
//        reservationDTO.setNoPeople(noPeople);
//
//        return reservationService.saveReservation(reservationDTO, userId, sportFacilityId);
//    }

    @PreAuthorize("hasAnyRole('ROLE_USER')")
    @PostMapping(value = "/saveReservation", produces = "application/json; charset=utf-8")
    @ResponseBody
    public ReservationDTO saveReservation(@RequestBody ReservationDTO reservationDTO){
        reservationDTO.setId(null);
        return reservationService.saveReservation(reservationDTO, reservationDTO.getUserDTO().getUserId(), reservationDTO.getSportFacilityDTO().getId());
    }


    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PutMapping("/updateReservation")
    @ResponseBody
    public ReservationDTO updateReservation(@RequestBody ReservationDTO reservationDTO){
//        ReservationDTO reservationDTO = new ReservationDTO();
//        reservationDTO.setId(id);
//        reservationDTO.setStartDate(startDate);
//        reservationDTO.setEndDate(endDate);
//        reservationDTO.setNoPeople(noPeople);

        return reservationService.updateReservation(reservationDTO.getId(), reservationDTO, reservationDTO.getUserDTO().getUserId(), reservationDTO.getSportFacilityDTO().getId());
    }

    @DeleteMapping("/deleteReservation")
    @ResponseBody
    public boolean deleteReservation(@RequestParam("id") Long id){
        return reservationService.deleteReservationDTO(id);
    }
}
