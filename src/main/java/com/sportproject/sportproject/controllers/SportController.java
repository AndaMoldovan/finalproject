package com.sportproject.sportproject.controllers;

import com.sportproject.sportproject.service.SportService;
import com.sportproject.sportproject.service.dto.SportDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/base/sport")
public class SportController {

    private final SportService sportService;

    @Autowired
    public SportController(SportService sportService){
        this.sportService = sportService;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    @GetMapping("/getAllSports")
    @ResponseBody
    public List<SportDTO> getAllSports(){
        return sportService.getAllSports();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    @GetMapping("/getSportById")
    @ResponseBody
    public SportDTO getSportById(@RequestParam("id") Long id){
        return sportService.getSportByIn(id);
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @PostMapping("/saveSport")
    @ResponseBody
    public SportDTO saveSport(@RequestBody SportDTO sportDTO){
        return sportService.saveSport(sportDTO);
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @PutMapping("/updateSport")
    @ResponseBody
    public SportDTO updateSport(@RequestBody SportDTO sportDTO){
        return sportService.updateSport(sportDTO.getSportId(), sportDTO);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @DeleteMapping("/deleteSport")
    @ResponseBody
    public boolean deleteSport(@RequestParam("id") Long id){
        return sportService.deleteSport(id);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @DeleteMapping("/deleteSportByName")
    @ResponseBody
    public boolean deleteSportByName(@RequestParam("name") String name){
        return sportService.deleteSportByName(name);
    }
}
