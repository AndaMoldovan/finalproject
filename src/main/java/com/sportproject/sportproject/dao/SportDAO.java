package com.sportproject.sportproject.dao;

import com.sportproject.sportproject.entities.Sport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface SportDAO extends JpaRepository<Sport, Long> {
    Sport findByName(String name);
}
