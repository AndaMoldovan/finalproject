package com.sportproject.sportproject.dao;

import com.sportproject.sportproject.entities.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface UserRoleDAO extends JpaRepository<UserRole, Long> {


}
