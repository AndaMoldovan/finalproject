package com.sportproject.sportproject.dao;

import com.sportproject.sportproject.entities.Facility;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface FacilityDAO extends JpaRepository<Facility, Long> {
    Facility findByAddress(String address);
}
