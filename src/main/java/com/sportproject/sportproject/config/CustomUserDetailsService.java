package com.sportproject.sportproject.config;

import com.sportproject.sportproject.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.sportproject.sportproject.entities.User;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<User> optionalUser = userDAO.findByEmail(email);

        optionalUser.orElseThrow(() -> new UsernameNotFoundException("Email not found"));

        return optionalUser
                .map(CustomUserDetails::new).get();
    }
}
